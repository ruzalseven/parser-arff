package ru.kpfu;

import java.util.*;

/**
 * Created by RYumaev on 18.04.2016.
 */
public class Relation {
    String name;
    List<Attribute> attributes = new ArrayList<>();
    Set<HashMap<String, Datum>> data = new HashSet<>();

    @Override
    public String toString() {
        return "Relation{\n" +
                "\t\t name='" + name + '\'' +
                ",\n\t\t attributes=" + attributes +
                ",\n\t\t data=" + data +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public Set<HashMap<String, Datum>> getData() {
        return data;
    }

    public void setData(Set<HashMap<String, Datum>> data) {
        this.data = data;
    }
}
