package ru.kpfu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RYumaev on 18.04.2016.
 */
public class Attribute<T> {
    String name;
    List<T> allowVars;
    Type type;

    public enum Type {
        bool("bool"),
        str("srt"),
        num("numeric");

        private String name;

        Type(String name) {
            this.name = name;
        }
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "name='" + name + '\'' +
                ", allowVars=" + allowVars +
                ", type=" + type +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<T> getAllowVars() {
        return allowVars;
    }

    public void setAllowVars(List<T> allowVars) {
        this.allowVars = allowVars;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}
