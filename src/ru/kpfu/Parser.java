package ru.kpfu;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by RYumaev on 18.04.2016.
 */
public class Parser {
    public static Relation readRelation(File in) throws IOException {
        if (in != null && !in.isDirectory() && in.exists()) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(in));
            Relation relation = new Relation();
            relation.setName(getRelationName(bufferedReader.readLine()));
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            while (!line.equals("")) {
                relation.attributes.add(getAttribute(line));
                line = bufferedReader.readLine();
            }
            line = bufferedReader.readLine();
            if (line.equals("@data")) {
                line = bufferedReader.readLine();
                while (!line.equals("")) {
                    relation.getData().add(getDatum(relation.attributes, line));
                    line = bufferedReader.readLine();
                }
                return relation;
            }

        }
        return null;
    }

    private static String getRelationName(String line) {
        if (line.contains("@relation")) {
            return line.substring(line.indexOf(' ') + 1);
        } else {
            throw new IllegalArgumentException("@relation");
        }
    }

    private static Attribute getAttribute(String line) {
        if (line.contains("@attribute")) {
            StringTokenizer stringTokenizer = new StringTokenizer(line, " ");
            stringTokenizer.nextToken();
            String name = stringTokenizer.nextToken();
            String token = stringTokenizer.nextToken();
            if (token.startsWith("{")) {
                List<String> allowVars = new ArrayList<>();
                String val = token.substring(1, token.length() - 1);
                switch (val) {
                    case "TRUE":
                    case "FALSE": {
                        Attribute<Boolean> attribute = new Attribute<>();
                        attribute.setName(name);
                        attribute.setType(Attribute.Type.bool);
                        return attribute;
                    }
                    default: {
                        Attribute<String> attribute = new Attribute<>();
                        attribute.setName(name);
                        attribute.setAllowVars(allowVars);
                        attribute.setType(Attribute.Type.str);
                        allowVars.add(val);
                        token = stringTokenizer.nextToken();
                        while (!token.endsWith("}") && stringTokenizer.hasMoreTokens()) {
                            allowVars.add(token.substring(0, token.length() - 1));
                            token = stringTokenizer.nextToken();
                        }
                        allowVars.add(token.substring(0, token.length() - 1));
                        return attribute;
                    }
                }
            } else {
                switch (token) {
                    case "numeric": {
                        Attribute<Long> attribute = new Attribute<>();
                        attribute.setName(name);
                        attribute.setType(Attribute.Type.num);
                        return attribute;
                    }
                    default: {
                        throw new IllegalArgumentException("@attribute on line: " + line);
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("@attribute on line: " + line);
        }
    }

    private static HashMap<String, Datum> getDatum(List<Attribute> attributes, String line) {
        if (line != null && attributes != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, ",");
            HashMap<String, Datum> datumHashMap = new HashMap<>();
            for (Attribute attribute : attributes) {
                switch (attribute.getType()) {
                    case bool: {
                        Datum<Boolean> datum = new Datum<>();
                        datum.setValue(Boolean.parseBoolean(tokenizer.nextToken()));
                        datumHashMap.put(attribute.getName(), datum);
                        break;
                    }
                    case num: {
                        Datum<Long> datum = new Datum<>();
                        datum.setValue(Long.parseLong(tokenizer.nextToken()));
                        datumHashMap.put(attribute.getName(), datum);
                        break;
                    }
                    case str: {
                        Datum<String> datum = new Datum<>();
                        datum.setValue(tokenizer.nextToken());
                        datumHashMap.put(attribute.getName(), datum);
                        break;
                    }
                }
            }
            return datumHashMap;
        }
        throw new IllegalArgumentException("attributes or line illegal: " + attributes + ", " + line);
    }
}
