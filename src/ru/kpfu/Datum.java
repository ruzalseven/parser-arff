package ru.kpfu;

/**
 * Created by RYumaev on 18.04.2016.
 */
public class Datum<T> {
    private T value;

    @Override
    public String toString() {
        return "Datum{" +
                "value=" + value +
                '}';
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
